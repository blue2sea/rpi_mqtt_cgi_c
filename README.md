# README #

This is Raspberry Pi MQTT, CGI Project with C.

### - How to run mqtt_publisher, mqtt_subscriber in RPi

    1. Copy MQTT-c-pub-sub-master_success folder to RPi.
    2. run mqtt_publisher to use shell command like this:
       cd MQTT-c-pub-sub-master_success/build
       ./mqttpub
    3. run mqtt_subscriber to use shell command like this:
       cd MQTT-c-pub-sub-master_success/build
       ./mqttsub
       
    Mqtt publish result is as follows:
![mqtt_publish](image/mqtt_publisher.PNG)
    Mqtt subscribe result is as follows:
![mqtt_subscribe](image/mqtt_subscriber.PNG)
       